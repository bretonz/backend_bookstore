package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.UserService;
import com.eht.training.bookstore.vo.BookVO;
import com.eht.training.bookstore.vo.UserVO;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/{user}")
public class UserCtrlImpl implements UserCtrl {

    @Override
    @PostMapping
    public void addUser(@RequestBody UserVO userVO){
      
//      UserVO user = new UserVO();
//      user.setUserId(this.userService.addUser(userVO));
      this.userService.addUser(userVO);
      
      
    }

    @Override
    @GetMapping
    public ArrayList<UserVO> getAllUsers(
            @RequestHeader( value= "EHT-Training-Token") String token,
            @RequestHeader( value = "hint", defaultValue = "") String hint,
            @RequestHeader( value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestHeader( value = "pageNumber", defaultValue = "1") Integer pageNumber ){
        
        return this.userService.getAllUser(token, hint, pageSize, pageNumber);
    }

    
    @Override
    @GetMapping(path = "/{userId}/book-loans")
    public ArrayList<BookVO> getallBookLoans(
            @RequestHeader( value= "EHT-Training-Token") String token, 
            @PathVariable Integer userId) {
        
        return this.userService.getallBookLoans(token, userId);
    }
    
    
    
    
    @Autowired
    private UserService userService;
    
}

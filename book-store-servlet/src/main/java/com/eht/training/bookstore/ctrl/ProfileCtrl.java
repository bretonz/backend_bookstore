/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.vo.UserVO;

/**
 *
 * @author fabian
 */
public interface ProfileCtrl {
    UserVO getProfile(String token);
    void updateUser(String token , UserVO userVO);
}

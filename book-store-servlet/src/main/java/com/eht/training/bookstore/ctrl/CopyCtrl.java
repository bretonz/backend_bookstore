/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

/**
 *
 * @author fabian
 */
public interface CopyCtrl {
    void deleteCopy(Integer id);
    void setLocation(Integer id , String location);
    void setStatus(Integer id, String status);
}

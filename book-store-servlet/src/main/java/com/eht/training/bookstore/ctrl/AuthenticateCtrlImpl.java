/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.AuthenticateUserService;
import com.eht.training.bookstore.vo.SessionVO;
import com.eht.training.bookstore.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = "/authenticate-user")
public class AuthenticateCtrlImpl implements AuthenticateCtrl {

    @Override
    @PostMapping
    public SessionVO authentication(
            @RequestBody UserVO user) {
       return this.authenticateUserService.authenticateUser(user);
    }
    
    @Autowired
    AuthenticateUserService authenticateUserService; 
    
}

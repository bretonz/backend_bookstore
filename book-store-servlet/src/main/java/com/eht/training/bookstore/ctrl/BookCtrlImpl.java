package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.BookService;
import com.eht.training.bookstore.vo.BookVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = "/book")
public class BookCtrlImpl implements BookCtrl {
    private static final Logger log = LoggerFactory.getLogger(BookCtrlImpl.class);

    @Override
    @GetMapping
    public  List<BookVO> getBookForHint(
            @RequestHeader String hint) {
        return this.bookService.getBooks(hint,1,2);
    }
    
    @PostMapping
    public void addBook(
            @RequestBody BookVO bookVO,
            @RequestHeader(value = "EHT-training-Token")String token) {
        log.info("Request for create Book {}", bookVO);
        this.bookService.addBook(bookVO, "123");
        
    }
    
    @Override    
    @GetMapping(path = "/{id}")
    public BookVO getById(
            @PathVariable Integer id) {
        log.trace("Request for get book {}" + id);
        return this.bookService.getById(id);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteBook(
            @PathVariable Integer id,
            @RequestHeader(value = "EHT-training-Token")String token) {
        log.trace("Request for delete");
        this.bookService.deleteBooK(id, token);
    }

    @Override
    @PostMapping( path = "/{id}/copy")
    public void saveCopyBook(
            @PathVariable Integer id) {
        log.trace("Request for save a copy of book");
        this.bookService.saveCopyBook(id);
    }

    
    
    
    
    @Autowired
    private BookService bookService;
}

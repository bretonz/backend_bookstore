/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.CopyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/copy")
public class CopyCtrlImpl implements CopyCtrl {

    @Override
    @DeleteMapping(path = "/{id}")
    public void deleteCopy(
            @PathVariable Integer id) {
        
        this.copyService.deleteCopy(id);
    }

    @Override
    @PutMapping(path = "/{id}/location")
    public void setLocation(
            @PathVariable Integer id, 
            @RequestHeader String location) {
        this.copyService.setLocation(id, location);
    }

    @Override
    @PutMapping(path = "/{id}/status")
    public void setStatus(
            @PathVariable Integer id, 
            @RequestHeader String status) {
        this.copyService.setStatus(id, status);
    }
    
    
    
    
    @Autowired
    private CopyService copyService;
    
}

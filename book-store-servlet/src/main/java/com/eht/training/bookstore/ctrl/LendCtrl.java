package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.vo.UserCopyVO;

/**
 *
 * @author fabian
 */
public interface LendCtrl {
        void lendBook(String token, Integer isbn, Integer userId, UserCopyVO userCopy);
}

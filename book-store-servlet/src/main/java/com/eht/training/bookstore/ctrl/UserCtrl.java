/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.vo.BookVO;
import com.eht.training.bookstore.vo.UserVO;
import java.util.ArrayList;

/**
 *
 * @author fabian
 */
public interface UserCtrl {
    void addUser(UserVO userVO);
    ArrayList<UserVO> getAllUsers(String token, String hint, Integer pagaSize, Integer pageNumber);
    ArrayList<BookVO> getallBookLoans(String token, Integer userId);
}

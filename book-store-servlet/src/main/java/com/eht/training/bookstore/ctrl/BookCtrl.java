/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.vo.BookVO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mbret
 */
public interface BookCtrl {
    void addBook(BookVO bookVO, String token);
    BookVO getById(Integer id);
    void deleteBook(Integer id, String token);
    void saveCopyBook(Integer id);
    List<BookVO> getBookForHint(String hint);
}

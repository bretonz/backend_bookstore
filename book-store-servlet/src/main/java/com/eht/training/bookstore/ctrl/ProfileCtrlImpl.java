/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.ProfileService;
import com.eht.training.bookstore.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = "/me/profile") 
public class ProfileCtrlImpl implements ProfileCtrl {

    @Override
    @GetMapping
    public UserVO getProfile(
            @RequestHeader String token) {
        
        return this.profileService.getProfile(token);
    }

    @Override
    @PutMapping
    public void updateUser(
            @RequestHeader String token, 
            @RequestBody UserVO userVO) {
        this.profileService.setProfile(token, userVO);
    
    }
    
    
    
    @Autowired
    private ProfileService profileService;
    
}

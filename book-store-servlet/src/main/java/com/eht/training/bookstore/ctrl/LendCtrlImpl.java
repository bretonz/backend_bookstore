package com.eht.training.bookstore.ctrl;

import com.eht.training.bookstore.service.LendService;
import com.eht.training.bookstore.vo.UserCopyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/lend-book")
public class LendCtrlImpl implements LendCtrl {

    @Override
    @PostMapping
    public void lendBook(
            @RequestHeader(value = "EHT-training-Token") String token, 
            @RequestHeader(value = "isbn") Integer isbn, 
            @RequestHeader(value = "userId") Integer userId,
            @RequestBody UserCopyVO userCopyVO) {
        this.lendService.lendBook(token, isbn, userId, userCopyVO);
    }
    
    @Autowired
    private LendService lendService;
}

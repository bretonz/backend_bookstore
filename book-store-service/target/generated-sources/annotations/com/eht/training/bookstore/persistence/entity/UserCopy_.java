package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-23T17:24:03")
@StaticMetamodel(UserCopy.class)
public class UserCopy_ { 

    public static volatile SingularAttribute<UserCopy, Date> returnDate;
    public static volatile SingularAttribute<UserCopy, String> returnComment;
    public static volatile ListAttribute<UserCopy, Copy> copyList;
    public static volatile SingularAttribute<UserCopy, Date> requestDate;
    public static volatile SingularAttribute<UserCopy, Date> maxreturnDate;
    public static volatile SingularAttribute<UserCopy, User> user;
    public static volatile SingularAttribute<UserCopy, Integer> loanId;

}
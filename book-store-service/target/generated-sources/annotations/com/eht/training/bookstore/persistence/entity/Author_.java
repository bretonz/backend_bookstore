package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Book;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-23T17:24:03")
@StaticMetamodel(Author.class)
public class Author_ { 

    public static volatile SingularAttribute<Author, String> author;
    public static volatile SingularAttribute<Author, Integer> authorId;
    public static volatile SingularAttribute<Author, String> lastname;
    public static volatile ListAttribute<Author, Book> bookList;

}
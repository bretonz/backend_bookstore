package com.eht.training.bookstore.persistence.entity;

import com.eht.training.bookstore.persistence.entity.Book;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-23T17:24:03")
@StaticMetamodel(Category.class)
public class Category_ { 

    public static volatile SingularAttribute<Category, String> category;
    public static volatile SingularAttribute<Category, Integer> categoryId;
    public static volatile ListAttribute<Category, Book> bookList;

}
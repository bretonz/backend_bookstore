/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fabian
 */
@Entity
@Table(name = "editorial")
@NamedQueries({
    @NamedQuery(name = "Editorial.findAll", query = "SELECT e FROM Editorial e"),
    @NamedQuery(name = "Editorial.findByEditorialId", query = "SELECT e FROM Editorial e WHERE e.editorialId = :editorialId"),
    @NamedQuery(name = "Editorial.findByEditorial", query = "SELECT e FROM Editorial e WHERE e.editorial = :editorial")})
public class Editorial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EDITORIAL_ID")
    private Integer editorialId;
    @Basic(optional = false)
    @Column(name = "EDITORIAL")
    private String editorial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "editorialId")
    private List<Book> bookList;

    public Editorial() {
    }

    public Editorial(Integer editorialId) {
        this.editorialId = editorialId;
    }

    public Editorial(Integer editorialId, String editorial) {
        this.editorialId = editorialId;
        this.editorial = editorial;
    }

    public Integer getEditorialId() {
        return editorialId;
    }

    public void setEditorialId(Integer editorialId) {
        this.editorialId = editorialId;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (editorialId != null ? editorialId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Editorial)) {
            return false;
        }
        Editorial other = (Editorial) object;
        if ((this.editorialId == null && other.editorialId != null) || (this.editorialId != null && !this.editorialId.equals(other.editorialId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.eht.training.bookstore.persistence.entity.Editorial[ editorialId=" + editorialId + " ]";
    }
    
}

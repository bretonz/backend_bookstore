package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.CopyDAO;
import com.eht.training.bookstore.persistence.dao.UserDAO;
import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.User;
import com.eht.training.bookstore.vo.UserVO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReciveServiceImpl implements ReciveService {

    @Override
    public void reciveBook(String token,UserVO userVO) {
        if(token.isEmpty()) 
            throw new IllegalAccessError("Invalid token");
        if(userVO.getUserId() <= 0) 
            throw new IllegalAccessError("Invalid user");
        
        User user  = this.userDAO.read(userVO.getUserId());
        Integer id = user.getLoanId().getLoanId();
        this.copyDAO.getCopyByIsbn(1);
        List<Copy> loans = this.copyDAO.getCopyByLoan(id);
        
        for (Copy copy : loans) {
            copy.setLoanId(null);
            copy.setStatusCopy("Available");
            this.copyDAO.update(copy);
        }
        //Verificar si se tiene que borrar de la tabla user_copy
        user.setLoanId(null);
        this.userDAO.update(user);
    }
    
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private CopyDAO copyDAO;
    
}

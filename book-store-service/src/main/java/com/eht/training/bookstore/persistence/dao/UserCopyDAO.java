/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;

import com.eht.training.bookstore.persistence.entity.Book;
import com.eht.training.bookstore.persistence.entity.UserCopy;
import java.util.List;

/**
 *
 * @author mbret
 */
public interface UserCopyDAO extends GenericDAO<UserCopy>{
    List<Book> getBooks(Integer isbn);
}

package com.eht.training.bookstore.persistence.entity;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author mbret
 */
@Entity
public class AuthorBookPK implements Serializable{

    private static final long serialVersionUID = -1119685904739946885L;
}

package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.SessionDAO;
import com.eht.training.bookstore.persistence.entity.Session;
import com.eht.training.bookstore.vo.SessionVO;
import com.eht.training.bookstore.vo.UserVO;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticateUserServiceImpl implements AuthenticateUserService {

    @Override
    public SessionVO authenticateUser(UserVO userVO) {
       SessionVO ss =new SessionVO();
       
       StringBuilder cad = new StringBuilder();
       cad.append(userVO.getEmail());
       cad.append( userVO.getPassword());
        
       Base64.Encoder encoder = Base64.getEncoder();
       String token = encoder.encodeToString(cad.toString().getBytes(StandardCharsets.UTF_8));
       
       Session session = this.sessionDAO.read(token);
       
       if(session == null) throw new IllegalAccessError("Invalid token");
       
       ss.setToken(token);
       
       return ss;
       
    }
    
    @Autowired
    SessionDAO sessionDAO;
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.vo;

/**
 *
 * @author fabian
 */
public class CopyVO {
     private int copyId;
    private String statusCopy;
    private String ubication;
    private BookVO isbn;
    private UserCopyVO loan;

    public int getCopyId() {
        return copyId;
    }

    public void setCopyId(int copyId) {
        this.copyId = copyId;
    }

    public String getStatusCopy() {
        return statusCopy;
    }

    public void setStatusCopy(String statusCopy) {
        this.statusCopy = statusCopy;
    }

    public String getUbication() {
        return ubication;
    }

    public void setUbication(String ubication) {
        this.ubication = ubication;
    }

    public BookVO getIsbn() {
        return isbn;
    }

    public void setIsbn(BookVO isbn) {
        this.isbn = isbn;
    }

    public UserCopyVO getLoan() {
        return loan;
    }

    public void setLoan(UserCopyVO loan) {
        this.loan = loan;
    }

    
}

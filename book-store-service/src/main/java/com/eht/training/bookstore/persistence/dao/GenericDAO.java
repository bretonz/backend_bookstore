package com.eht.training.bookstore.persistence.dao;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author mbret
 */
public interface GenericDAO<T extends Object> {
    List<T> getAll();
    Serializable create(T entity);
    T read(Serializable id);
    void update(T entity);
    void delete(Serializable id);
}

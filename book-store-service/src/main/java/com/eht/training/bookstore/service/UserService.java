package com.eht.training.bookstore.service;

import com.eht.training.bookstore.vo.BookVO;
import com.eht.training.bookstore.vo.UserVO;
import java.util.ArrayList;

/**
 *
 * @author fabian
 */
public interface UserService {
    void addUser(UserVO userVO);
    ArrayList<UserVO> getAllUser(String token,String hint, Integer pageSize, Integer pageNumber);
    ArrayList<BookVO> getallBookLoans(String token, Integer userId);
    
}

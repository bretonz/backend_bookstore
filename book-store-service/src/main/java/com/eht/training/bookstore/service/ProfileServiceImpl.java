package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.SessionDAO;
import com.eht.training.bookstore.persistence.dao.UserDAO;
import com.eht.training.bookstore.persistence.entity.Session;
import com.eht.training.bookstore.persistence.entity.User;
import com.eht.training.bookstore.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Override
    public UserVO getProfile(String token) {
        if(token == null || token.equals("")) throw new IllegalAccessError("Token invalid");
        
        Session session = this.sessionDAO.read(token);
        if(session == null) throw new IllegalAccessError("User does not found");
        
        User user  = this.userDAO.read(session.getUserId().getUserId());
        
        UserVO vo = new UserVO();
        vo.setUserId(user.getUserId());
        vo.setEmail(user.getEmail());
        vo.setUserName(user.getUsername());
        vo.setLastName(user.getLastname());
        vo.setPhoneNumber(user.getPhonenumber());
        
        return vo;
    }

    @Override
    public void setProfile(String token, UserVO userVO) {
        if(token == null || token.equals("")) throw new IllegalAccessError("Token invalid");
        
        Session session = this.sessionDAO.read(token);
        if(session == null) throw new IllegalAccessError("User does not found");
        
        User user = this.userDAO.read(session.getUserId().getUserId());
        
        if(userVO.getEmail() != null){
            user.setEmail(userVO.getEmail());
        }
        if(userVO.getUserName() != null){
            user.setUsername(userVO.getUserName());
        }
        if(userVO.getLastName() != null){
            user.setLastname(userVO.getLastName());
        }
        if(userVO.getPhoneNumber() != null){
            user.setPhonenumber(userVO.getPhoneNumber());
        }
        if(userVO.getPassword() != null){
            user.setPassword(userVO.getPassword());
        }
        
        user.setPhotoUrl(userVO.getPhotoUrl());
        
        
        this.userDAO.update(user);
    }
    
    @Autowired
    private SessionDAO sessionDAO;
    @Autowired
    private UserDAO userDAO;
}

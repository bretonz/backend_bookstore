package com.eht.training.bookstore.service;

import com.eht.training.bookstore.vo.BookVO;
import com.eht.training.bookstore.vo.UserCopyVO;



/**
 *
 * @author fabian
 */
public interface LendService {
    void lendBook(String token,Integer isbn, Integer userId, UserCopyVO userCopyVO);
}

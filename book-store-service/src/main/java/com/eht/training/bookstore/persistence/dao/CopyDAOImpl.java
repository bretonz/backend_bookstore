package com.eht.training.bookstore.persistence.dao;


import com.eht.training.bookstore.persistence.entity.Copy;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CopyDAOImpl extends CrudDAO<Copy> implements CopyDAO {

    @Override
    public List<Copy> getCopyByIsbn(Integer isbn) {
        String status = "Available";
        Query query = this.session.getCurrentSession()
                .createQuery("From Copy where isbn.isbn = :id and status_copy = :statusCode")
                .setParameter("id", isbn)
                .setParameter("statusCode", status);
        
        List<Copy> copy =  query.getResultList();
         
        return copy;
    }

    @Override
    public List<Copy> getCopyByLoan(Integer loand) {
       Query query = this.session.getCurrentSession()
               .createQuery("From Copy where loan_id = :loanId")
               .setParameter("loanId", loand);
       
       List<Copy> loan = query.getResultList();
       
       return loan;
    }
    
    
    
    @Autowired
    private SessionFactory session;
}

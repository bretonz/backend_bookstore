package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.BookDAO;
import com.eht.training.bookstore.persistence.dao.RoleDAO;
import com.eht.training.bookstore.persistence.dao.SessionDAO;
import com.eht.training.bookstore.persistence.dao.UserDAO;
import com.eht.training.bookstore.persistence.dao.UserRoleDAO;
import com.eht.training.bookstore.persistence.entity.Author;
import com.eht.training.bookstore.persistence.entity.Book;
import com.eht.training.bookstore.persistence.entity.Category;
import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.Editorial;
import com.eht.training.bookstore.persistence.entity.Session;
import com.eht.training.bookstore.persistence.entity.User;
import com.eht.training.bookstore.persistence.entity.UserCopy;
import com.eht.training.bookstore.vo.AuthorVO;
import com.eht.training.bookstore.vo.BookVO;
import com.eht.training.bookstore.vo.CategoryVO;
import com.eht.training.bookstore.vo.EditorialVO;
import com.eht.training.bookstore.vo.UserVO;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    @Override
    @Transactional(readOnly = false)
    //verificar si el usuario no existe  
    public void addUser(UserVO userVO) {
        if (userVO.getEmail().length() <= 0) 
            throw new IllegalArgumentException("Email invalid");
        if (userVO.getUserName().length() <=0)
            throw new IllegalArgumentException("UserName invalid");
        if (userVO.getLastName().length() <= 0) 
            throw new IllegalArgumentException("LastName invalid");
        if (userVO.getPhoneNumber() == 0) 
            throw new IllegalArgumentException("PhoneNumber invalid");
        if (userVO.getPassword().length() <= 0) 
            throw new IllegalArgumentException("Password invalid");
          
        User user = new User();
        user.setEmail(userVO.getEmail());
        user.setUsername(userVO.getUserName());
        user.setLastname(userVO.getLastName());
        user.setPhonenumber(userVO.getPhoneNumber());
        user.setPassword(userVO.getPassword());
        user.setPhotoUrl(userVO.getPhotoUrl());
       
        Integer id = (Integer) this.userDAO.create(user);
        
        
        StringBuilder cad = new StringBuilder();
        cad.append(user.getEmail());
        cad.append( user.getPassword());
        
        Base64.Encoder encoder = Base64.getEncoder();
        String token = encoder.encodeToString(cad.toString().getBytes(StandardCharsets.UTF_8));
      
        Session session = new Session();
        session.setToken(token);
        session.setUserId(user);
        this.sessionDAO.create(session);
        
        
        
        
    }

    @Override
    @Transactional(readOnly = true)
    public ArrayList<UserVO> getAllUser(String token, String hint, Integer pageSize, Integer pageNumber) {
        if (token == null || token.length() <=0 ) 
            throw new IllegalArgumentException("Token invalid");
        //Validar si existe token en table SESSION
        //falta validar los parametros-hint-pagaSize-pageNumber
        ArrayList<UserVO> usersVO = new ArrayList<>();

        for(User currentUser : this.userDAO.getAll()){
            UserVO vo = new UserVO();
            vo.setUserId(currentUser.getUserId());
            vo.setUserName(currentUser.getUsername());
            vo.setLastName(currentUser.getLastname());
            vo.setEmail(currentUser.getEmail());
            vo.setPhoneNumber(currentUser.getPhonenumber());
            vo.setPhotoUrl(currentUser.getPhotoUrl());
            usersVO.add(vo);
        }
        return usersVO;
    }

    @Override
    @Transactional(readOnly = true)
    public ArrayList<BookVO> getallBookLoans(String token, Integer userId) {
        if (token == null || token.length() <=0 ) 
            throw new IllegalArgumentException("Token invalid");
         //validar el token
        if (userId <=0) 
            throw new IllegalArgumentException("User ID invalid");
        
        User user = this.userDAO.read(userId);
        if (user == null) 
            throw new NullPointerException("User does not exist");
       
        UserCopy loand = user.getLoanId();
        
        @SuppressWarnings("null")
        ArrayList<BookVO> bookVO = new ArrayList<>();
        
        for(Copy copyCurrent : loand.getCopyList()){
            int isbn = copyCurrent.getIsbn().getIsbn();
            for(Book book : this.bookDAO.getAll()){
                if (book.getIsbn() == isbn) {
                    BookVO vo = new BookVO();
                    vo.setIsbn(book.getIsbn());
                    vo.setTitle(book.getTitle());
                        ArrayList<AuthorVO> authors =  new ArrayList<>();
                        for (Author authorCurrent : book.getAuthorList()) {
                            AuthorVO authorVO = new AuthorVO();
                            
                            authorVO.setAuthor_Id(authorCurrent.getAuthorId());
                            authorVO.setAuthor(authorCurrent.getAuthor());
                            authorVO.setLastName(authorCurrent.getLastname());
                            
                            authors.add(authorVO);
                        }
                    vo.setAuthors(authors);
                    
                    Category categoryCurrent = book.getCategoryId(); 
                    CategoryVO categoryVO = new CategoryVO();
                    categoryVO.setCategory_Id(categoryCurrent.getCategoryId());
                    categoryVO.setCategory(categoryCurrent.getCategory());
                    vo.setCategory(categoryVO);
                    
                    Editorial editorialCurrent = book.getEditorialId();
                    EditorialVO editorialVO = new EditorialVO();
                    editorialVO.setEditorial_Id(editorialCurrent.getEditorialId());
                    editorialVO.setEditorial(editorialCurrent.getEditorial());
                    vo.setEditorial(editorialVO);
                    
                    bookVO.add(vo);  
                }
            }
        }
       return bookVO; 
    }
    
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private BookDAO bookDAO;
    @Autowired
    private SessionDAO sessionDAO;
    @Autowired
    private RoleDAO RoleDAO;
    @Autowired
    private UserRoleDAO userRoleDAO;
}

package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.CopyDAO;
import com.eht.training.bookstore.persistence.entity.Copy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CopyServiceImpl implements CopyService {

    @Override
    public void deleteCopy(Integer copyId) {
        if(copyId <=0) 
            throw new IllegalArgumentException("Error id");
        this.copyDAO.delete(copyId);
    }

    @Override
    public void setLocation(Integer copyId, String location) {
        if(copyId <=0) 
            throw new IllegalArgumentException("Error id");
        if(location.isEmpty()) 
            throw new IllegalArgumentException("Location invalid"); 
        Copy copy = this.copyDAO.read(copyId);
        
        copy.setUbication(location);
        this.copyDAO.update(copy);
        
    }

    @Override
    public void setStatus(Integer copyId, String status) {
        if(copyId <= 0) 
            throw new IllegalArgumentException("Error id");
        if(status.isEmpty()) 
            throw new IllegalAccessError("Status empty");
        if(status.equals("Available") 
                || status.equals("Occupied") 
                || status.equals("Repairing")){
            
            Copy copy = this.copyDAO.read(copyId);
            copy.setStatusCopy(status);
            this.copyDAO.update(copy);
        }else{
             throw new IllegalAccessError("Status Invalid");    
        }
               
    }
    
    @Autowired
    CopyDAO copyDAO;
    
}

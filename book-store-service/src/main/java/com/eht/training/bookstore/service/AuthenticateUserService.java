package com.eht.training.bookstore.service;

import com.eht.training.bookstore.vo.SessionVO;
import com.eht.training.bookstore.vo.UserVO;
/**
 *
 * @author fabian
 */
public interface AuthenticateUserService {
    SessionVO authenticateUser(UserVO userVO);
}

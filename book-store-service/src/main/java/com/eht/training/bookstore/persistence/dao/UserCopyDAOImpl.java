package com.eht.training.bookstore.persistence.dao;

import com.eht.training.bookstore.persistence.entity.Book;
import com.eht.training.bookstore.persistence.entity.UserCopy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository
@Transactional
public class UserCopyDAOImpl extends CrudDAO<UserCopy> implements UserCopyDAO {

    @Override
    public List<Book> getBooks(Integer isbn) {
       return this.sessionFactory.getCurrentSession()
               .createQuery(" From book WHERE ISBN =  :isbn")
               .setParameter("isbn", isbn)
               .list();
    }
    
    
    
    @Autowired
    protected SessionFactory sessionFactory; 
    
}

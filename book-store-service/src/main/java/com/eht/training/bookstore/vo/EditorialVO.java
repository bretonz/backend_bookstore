/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.vo;

/**
 *
 * @author fabian
 */
public class EditorialVO {
     private int editorial_Id;
    private String editorial;

    public int getEditorial_Id() {
        return editorial_Id;
    }

    public void setEditorial_Id(int editorial_Id) {
        this.editorial_Id = editorial_Id;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    
    
}

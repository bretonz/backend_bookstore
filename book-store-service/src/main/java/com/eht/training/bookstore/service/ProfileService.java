package com.eht.training.bookstore.service;

import com.eht.training.bookstore.vo.UserVO;

/**
 *
 * @author fabian
 */
public interface ProfileService {
    UserVO getProfile(String token);
    void setProfile(String token, UserVO userVO);
}

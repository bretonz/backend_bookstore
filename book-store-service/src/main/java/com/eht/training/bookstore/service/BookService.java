package com.eht.training.bookstore.service;

import com.eht.training.bookstore.vo.BookVO;
import java.util.List;

/**
 *
 * @author mbret
 */
public interface BookService {
    void addBook(BookVO bookVO, String token);
    List<BookVO> getBooks(String hint, Integer pageSize, Integer pageNumber);
    BookVO getById(Integer id);
    void deleteBooK(Integer id, String token);
    void saveCopyBook(Integer id);
}

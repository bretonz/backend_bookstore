package com.eht.training.bookstore.service;

/**
 *
 * @author fabian
 */
public interface CopyService {
    void deleteCopy(Integer copyId);
    void setLocation(Integer copyId, String location);
    void setStatus(Integer copyId, String status);
}

package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.AuthorDAO;
import com.eht.training.bookstore.persistence.dao.BookDAO;
import com.eht.training.bookstore.persistence.dao.CategoryDAO;
import com.eht.training.bookstore.persistence.dao.CopyDAO;
import com.eht.training.bookstore.persistence.dao.EditorialDAO;
import com.eht.training.bookstore.persistence.entity.Author;
import com.eht.training.bookstore.persistence.entity.Book;
import com.eht.training.bookstore.persistence.entity.Category;
import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.Editorial;
import com.eht.training.bookstore.vo.AuthorVO;
import com.eht.training.bookstore.vo.BookVO;
import com.eht.training.bookstore.vo.CategoryVO;
import com.eht.training.bookstore.vo.EditorialVO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookServiceImpl implements BookService {

    @Override
    @Transactional(readOnly = false)
    public void addBook(BookVO bookVO, String token) {
        if(bookVO.getIsbn() <= 0) 
            throw new IllegalArgumentException("Invalid isbn");
        
        Book bookExist = this.bookDAO.read(bookVO.getIsbn());
        if (bookExist != null) throw new Error("Book already exist");
        
        if(bookVO.getTitle() == null 
                || bookVO.getTitle().isEmpty()
                || bookVO.getTitle().length() > 30) 
            throw new IllegalArgumentException("Invalid title"); 
       
        int category_id = bookVO.getCategory().getCategory_Id();
        Category category = this.categoryDAO.read(category_id);        
            if (category == null) 
                throw new NullPointerException("Category not found");
       
        Editorial editorial = this.editorialDAO.read(bookVO.getEditorial().getEditorial_Id());
            if (editorial == null ) 
                throw new NullPointerException("Editorial not found");
        
        List<Author> authors =  new ArrayList<>();
        for (AuthorVO authorVO : bookVO.getAuthors()) {
            Author author = new Author();
            //author.setAuthorId(authorVO.getAuthor_Id());
//            author.setAuthor(authorVO.getAuthor());
//            author.setLastname(authorVO.getLastName());
            if(authorVO.getAuthor_Id() == 0){
                //Creando Author
               // Author authorDB = new Author();
                author.setAuthor(authorVO.getAuthor());
                author.setLastname(authorVO.getLastName());
                
                Integer id =(Integer)this.authorDAO.create(author);
                authors.add(this.authorDAO.read(id));
            }else{
                authors.add(this.authorDAO.read(author.getAuthorId()));
                }
            }
        
        Book book = new Book();  
        book.setIsbn(bookVO.getIsbn());
        book.setTitle(bookVO.getTitle());
        book.setCategoryId(category);
        book.setAuthorList(authors);
        book.setEditorialId(editorial);
        
        this.bookDAO.create(book);
    }
    
    @Override
    @Transactional(readOnly = true)
    public BookVO getById(Integer id) {
        if(id == null || id <= 0)
            throw new IllegalArgumentException("INVALID ID");
            
        Book entity = this.bookDAO.read(id);
         if(entity == null)
             throw new NullPointerException("NOT FOUND");
         
        BookVO book = new BookVO();
        book.setIsbn(entity.getIsbn());
        book.setTitle(entity.getTitle());
         
        List<AuthorVO> authors = new ArrayList<>();
        for(Author currentAuthor : entity.getAuthorList()){
            AuthorVO current = new AuthorVO();
            current.setAuthor_Id(currentAuthor.getAuthorId());
            current.setAuthor(currentAuthor.getAuthor());
            current.setLastName(currentAuthor.getLastname());
             
            authors.add(current);
        }
         
        book.setAuthors(authors);
        
        Category current = entity.getCategoryId();
        CategoryVO currentCategory = new CategoryVO();
        currentCategory.setCategory_Id(current.getCategoryId());
        currentCategory.setCategory(current.getCategory());
        book.setCategory(currentCategory);
         
        Editorial editorial = entity.getEditorialId();
        EditorialVO currentEditorial = new EditorialVO();
        currentEditorial.setEditorial_Id(editorial.getEditorialId());
        currentEditorial.setEditorial(editorial.getEditorial());
        
        book.setEditorial(currentEditorial);
        
        return book;
    }
    
    
    @Override
    public void deleteBooK(Integer id, String token) {
        if (id == null || id <0) 
            throw new IllegalArgumentException("Id invalid");
        
        Book book =  this.bookDAO.read(id);
        if (book == null) 
            throw new NullPointerException("Book not exist");
        
        this.bookDAO.delete(id);
    }

    @Override
    public void saveCopyBook(Integer id) {
        if (id == null || id <= 0) 
            throw new IllegalArgumentException("Id invalid");
        
        Book book = this.bookDAO.read(id);
        
        if (book == null) 
            throw new NullPointerException("Book not found");
        
        Copy copy = new Copy();
        copy.setIsbn(book);
          
        this.copyDAO.create(copy);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BookVO> getBooks(String hint, Integer pageSize, Integer pageNumber) {
        List books = new ArrayList();
        if(hint.equals("null") || hint == null){
            //books = this.bookDAO.getAll();
            for (Book CurrentBook : this.bookDAO.getAll()) {
                BookVO vo = new BookVO();
                vo.setIsbn(CurrentBook.getIsbn());
                vo.setTitle(CurrentBook.getTitle());
                
                @SuppressWarnings("null")
                List<AuthorVO> authors = new ArrayList<>();
                for(Author currentAuthor : CurrentBook.getAuthorList()){
                    AuthorVO current = new AuthorVO();
                    current.setAuthor_Id(currentAuthor.getAuthorId());
                    current.setAuthor(currentAuthor.getAuthor());
                    current.setLastName(currentAuthor.getLastname());

                    authors.add(current);
                }
                vo.setAuthors(authors); 
                
                Category current = CurrentBook.getCategoryId();
                CategoryVO currentCategory = new CategoryVO();
                currentCategory.setCategory_Id(current.getCategoryId());
                currentCategory.setCategory(current.getCategory());
                vo.setCategory(currentCategory);

                Editorial editorial = CurrentBook.getEditorialId();
                EditorialVO currentEditorial = new EditorialVO();
                currentEditorial.setEditorial_Id(editorial.getEditorialId());
                currentEditorial.setEditorial(editorial.getEditorial());

                vo.setEditorial(currentEditorial);
                
            books.add(vo);
            }
        }else{
            //books = this.bookDAO.getBookForHint(hint);
            
            for (Book CurrentBook : this.bookDAO.getBookForHint(hint)) {
                BookVO vo = new BookVO();
                vo.setIsbn(CurrentBook.getIsbn());
                vo.setTitle(CurrentBook.getTitle());
                
                @SuppressWarnings("null")
                List<AuthorVO> authors = new ArrayList<>();
                for(Author currentAuthor : CurrentBook.getAuthorList()){
                    AuthorVO current = new AuthorVO();
                    current.setAuthor_Id(currentAuthor.getAuthorId());
                    current.setAuthor(currentAuthor.getAuthor());
                    current.setLastName(currentAuthor.getLastname());

                    authors.add(current);
                }
                vo.setAuthors(authors); 
                
                Category current = CurrentBook.getCategoryId();
                CategoryVO currentCategory = new CategoryVO();
                currentCategory.setCategory_Id(current.getCategoryId());
                currentCategory.setCategory(current.getCategory());
                vo.setCategory(currentCategory);

                Editorial editorial = CurrentBook.getEditorialId();
                EditorialVO currentEditorial = new EditorialVO();
                currentEditorial.setEditorial_Id(editorial.getEditorialId());
                currentEditorial.setEditorial(editorial.getEditorial());

                vo.setEditorial(currentEditorial);
                
            books.add(vo);
            }
            
        }
       
        
       return books;
    }
    
    
    @Autowired
    private BookDAO bookDAO;
    @Autowired
    private CategoryDAO categoryDAO;
    @Autowired
    private EditorialDAO editorialDAO;
    @Autowired
    private CopyDAO copyDAO;
    @Autowired
    private  AuthorDAO authorDAO;
}

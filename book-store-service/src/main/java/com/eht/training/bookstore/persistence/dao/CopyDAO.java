/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;


import com.eht.training.bookstore.persistence.entity.Copy;
import java.util.List;

/**
 *
 * @author mbret
 */
public interface CopyDAO extends GenericDAO<Copy>{
    List<Copy> getCopyByIsbn(Integer isbn);
    List<Copy> getCopyByLoan(Integer loand);
}

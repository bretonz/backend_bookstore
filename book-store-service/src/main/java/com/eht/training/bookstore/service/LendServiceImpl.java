package com.eht.training.bookstore.service;

import com.eht.training.bookstore.persistence.dao.CopyDAO;
import com.eht.training.bookstore.persistence.dao.UserCopyDAO;
import com.eht.training.bookstore.persistence.dao.UserDAO;
import com.eht.training.bookstore.persistence.entity.Copy;
import com.eht.training.bookstore.persistence.entity.User;
import com.eht.training.bookstore.persistence.entity.UserCopy;
import com.eht.training.bookstore.vo.UserCopyVO;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LendServiceImpl implements LendService {

    @Override
    public void lendBook(String token, Integer isbn, Integer userId, UserCopyVO userCopyVO) {
        if(token.isEmpty()) 
            throw new IllegalAccessError("Invalid token");
        if(isbn <= 0 ) 
            throw new IllegalArgumentException("Invalid token");
        if(userId <= 0 ) 
            throw new IllegalArgumentException("Invalid user");
        
        User user = this.userDAO.read(userId);
        List<Copy> loans= this.copyDAO.getCopyByLoan(user.getLoanId().getLoanId());
        
        if (loans.size() > 3) 
            throw new IllegalAccessError("User has 3 loans");
        
        Calendar calendar = Calendar.getInstance();
        java.sql.Date ourJavaDateObject = new java.sql.Date(calendar.getTime().getTime());

        List<Copy> copyTable  = this.copyDAO.getCopyByIsbn(isbn);
        for (Copy CurrenBook : copyTable) {
            UserCopy userCopy = new UserCopy();
            userCopy.setRequestDate(ourJavaDateObject);
        //  userCopy.setReturnDate(userCopyVO.getReturnDate());
        //  userCopy.setMaxreturnDate(userCopyVO.getMaxReturnDate());           
            userCopy.setReturnDate(ourJavaDateObject);
            userCopy.setMaxreturnDate(ourJavaDateObject);
            userCopy.setReturnComment(userCopyVO.getReturnComment());
            Integer uC =(Integer)this.userCopyDAO.create(userCopy);

            Copy copy = new Copy();
            copy.setCopyId(CurrenBook.getCopyId());
            copy.setStatusCopy("Occupied");
            copy.setUbication(CurrenBook.getUbication());
            copy.setIsbn(CurrenBook.getIsbn());
            copy.setLoanId(this.userCopyDAO.read(uC));
            this.copyDAO.update(copy);
                    
            user.setLoanId(userCopy);
            this.userDAO.update(user);

            break;
        }  
    }
    
    @Autowired
    private CopyDAO copyDAO;
    @Autowired
    private UserCopyDAO userCopyDAO;
    @Autowired
    private UserDAO userDAO;
}

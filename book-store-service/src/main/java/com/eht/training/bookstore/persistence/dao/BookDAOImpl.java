/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.bookstore.persistence.dao;

import com.eht.training.bookstore.persistence.entity.Book;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BookDAOImpl extends CrudDAO<Book> implements BookDAO {

    @Override
    public List<Book> getBookForHint(String hint) {
        Query query = this.sessionFactory.getCurrentSession()
                .createQuery("From Book where title like :hint")
                .setParameter("hint", "%"+hint+"%");
        
                //List<Book> books = query.getResultList();
                List<Book> books = query.getResultList();
        return books;        
    }
    
    @Autowired
    protected SessionFactory sessionFactory; 
    
}
